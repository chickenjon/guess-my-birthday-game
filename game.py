from random import randint

guess = False
final_month = 0
final_year = 0
month_lower = 1
month_upper = 12
year_lower = 1924
year_upper = 2004
month = randint(month_lower, month_upper)
year = randint(year_lower, year_upper)

name = input("\nHi! What's your name?\n\n")

for n in range(5):
    answer = input("\nGuess #" + str(n + 1) + ": " + name + ", were you born in " + str(month) + "/" + str(year) + "?\nEnter yes or no.\n\n")
    if answer == "yes":
        guess = True
        final_month = month
        final_year = year
        print("\nI knew it!\n")
        break

    elif answer == "no":
        if n == 4:
            break
        print("\nDrat! Lemme try again!")
        if final_month != month:
            month_answer = input("\nWas the month correct? Or were your born in an earlier or later month?\nType in yes, earlier, or later.\n\n")
            if month_answer == "yes":
                final_month = month
            elif month_answer == "earlier":
                month_upper = month - 1
                month = randint(month_lower, month_upper)
            elif month_answer == "later":
                month_lower = month + 1
                month = randint(month_lower, month_upper)
            else:
                print("\nHuh? You're weird.\n")
                quit()

        if final_year != year:
            year_answer = input("\nWas the year correct? Or were your born in an earlier or later year?\nType in yes, earlier, or later.\n\n")
            if year_answer == "yes":
                final_year = year
            elif year_answer == "earlier":
                year_upper = year - 1
                year = randint(year_lower, year_upper)
            elif year_answer == "later":
                year_lower = year + 1
                year = randint(year_lower, year_upper)
            else:
                print("\nHuh? You're weird.\n")
                quit()
    
    else:
        print("\nHuh? You're weird.\n")
        quit()

if guess == True:    
    print("You were born in " + str(final_month) + "/" + str(final_year) + ".\n")
else:
    print("\nI have other things to do. Good bye.\n")